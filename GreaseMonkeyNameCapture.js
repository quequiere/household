// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.buyspares.co.uk/cooker-oven/knob/catalogue.pl?path=462784:496321&refine=knob*
// @require     https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.js
// @grant   GM_getValue
// @grant   GM_setValue
// @grant GM_xmlhttpRequest
// ==/UserScript==

(function() {
   console.log("Start script");

   var nextId = GM_getValue("curentID", 0) + 1
   routine(nextId);


})();


function routine(pageId)
{
    getData();
    var scan = true;

    console.log("Page id is "+pageId);
    if(pageId % 100 == 0)
    {
        scan = confirm("Continue scan ?");
    }

    if(!scan)
    {
        var list = GM_getValue("plist", [""])
        var blob = new Blob(list, {type: "text/plain;charset=utf-8"});
        saveAs(blob, "hello world.txt");
        GM_setValue("plist", [""]);
        GM_setValue("curentID", 1);
    }
    else
    {
        GM_setValue("curentID", pageId);
        window.location = "https://www.buyspares.co.uk/cooker-oven/knob/catalogue.pl?path=462784:496321&refine=knob&page="+pageId;
    }


}

function getData(){

     var list = GM_getValue("plist", [""]);

    var r = document.getElementsByClassName('catalogue-product-text-main')
    for(var i= 0; i < r.length; i++)
    {
        var name = r[i].firstChild.textContent.trim();
        name = name+"#";
        list.push(name);
    }
    GM_setValue("plist", list);
}

