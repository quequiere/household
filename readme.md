# SupHouseHold

Suphousehold est un site de vente en ligne de pièces détachées. Il est issu de la matière 3JVA proposée par Supinfo

# Run

Pour lancer le site web en local, rien de plus simple.
Commencez par installer TomCat 7 sur votre ordinateur.
Puis lancer les commandes dans une invite de commande en administrateur
> gradlew clean build

> gradlew tomcatRunWar

Le site devrait être consultable depuis http://localhost:8080/houseHold/

Utilisez la commande suivante pour killer l'instance du serveur

> gradlew tomcatStop

## Sources

Plusieurs extraits de code on été utilisés pour réaliser ce projet.
Parmi ceux ci, des extraits de codes venant des labs effectués avec le professeur.
Et d'autres extraits provenant de mes multiples recherches. Plusieurs bootstrap snippets ont été utlisés pour le front-end de ce projet

## Nommage des pièces détachées

La totalité des noms des pièces détachées sont réelles et proviennent de https://www.buyspares.co.uk
Les noms ont été extraits par un script greasemonkey dont je JS est joins à ce projet.


## Database
Au premier démarrage la database s'initialise avec toutes les pièces nécessaires. Cela étant directement issue de l'extraction citée ci dessus. La database est stockée en SQLite.
Elle se trouvera dans votre path d'execution, ou bien dans votre JDK si vous utilisez intellij ou eclipse pour executer ce projet.

Vous pouvez bien entendu changer une base MySQL pour ce projet, en effectuant les modifications nécessaires dans le fichiers persistence.xml

J'ai volontairement utilsé une base SQLite pour que le projet soit executable immédiatement sure n'importe quelle machine sans avoir besoin d'installer un serveur SQL.

## API
L'api se trouve à la racine du site http://localhost:8080/houseHold/api/items

## Outils
L'ensemble de ce projet a été réalisé sous IntelliJ ultimate et gradle

## Repository git
L'ensemble du projet a été versionné avec git. L'évolution du travail peut être visionné par l'évaluateur dans le dossier .git

Le projet est privé et n'a pas été rendu public.