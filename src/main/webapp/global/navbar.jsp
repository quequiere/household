<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#search" ).autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    url: "${pageContext.request.contextPath}/api/items/searchName/"+request.term,
                    success: function( data ) {
                        response( data );
                    }
                } );
            },
            minLength: 2,
            select: function( event, ui ) {
                console.log( "Selected: " + ui.item.value);
            }
        } );
    } );
</script>





<base href="${request.contextPath}" />

<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="${pageContext.request.contextPath}/">SupHouseHold</a>
    </div>


    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/">Home</a></li>
        </ul>
        <div class="col-sm-3 col-md-3">
            <form  method="get" class="navbar-form" role="search" action="${pageContext.request.contextPath}/product/itemsearch">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Dynamic search here" name="iname" id="search">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <ul class="nav navbar-nav navbar-right">


            <c:if test="${empty sessionScope.username}">
                <li><a href="${pageContext.request.contextPath}/auth/login">Login</a></li>
                <li><a href="${pageContext.request.contextPath}/auth/register">Register</a></li>
            </c:if>

            <c:if test="${not empty sessionScope.username}">
                <li><a href="${pageContext.request.contextPath}/auth/secure/profile">Your profile</a></li>
                <li><a href="${pageContext.request.contextPath}/auth/secure/basket">Cart</a></li>
                <li><a href="${pageContext.request.contextPath}/auth/secure/history">History</a></li>
                <li><a href="${pageContext.request.contextPath}/auth/logout">Logout</a></li>
            </c:if>


        </ul>
    </div>
</nav>