<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="../global/header.jsp" %>


<c:if test="${not empty fail}">
    <h1 class="mainTit" style="text-align: center">${fail}</h1>
</c:if>

<c:if test="${ empty fail}">


    <div class="container">

        <c:forEach items="${iresList}" var="item">
            <div class="col-xs-6 col-md-6">
                <div class="product-content product-wrap clearfix">
                    <div class="row">
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="product-image">
                                <a href="${pageContext.request.contextPath}/product/itemdetail?itemid=<c:out value="${item.id}"/>">
                                    <img src="https://via.placeholder.com/300x300?text=ProductId <c:out value="${ item.id } image " />" class="img-responsive">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12 col-xs-12">
                            <div class="product-deatil">
                                <h5 class="name">
                                    <a href="${pageContext.request.contextPath}/product/itemdetail?itemid=<c:out value="${item.id}"/>">
                                        <c:out value="${ item.name }"/>
                                    </a>
                                </h5>
                                <p class="price-container">
                                    <span> <c:out value="${ item.price } €"/></span>
                                </p>
                                <span class="tag1"></span>
                            </div>
                            <div class="description">
                                <p>This is the description for product <c:out value="${ item.name }"/></p>
                            </div>
                            <div class="product-info smart-form">
                                <div class="row">
                                    <%--<div class="col-md-6 col-sm-6 col-xs-12">--%>
                                        <%--<a href="javascript:void(0);" class="btn btn-success">Add to cart</a>--%>
                                    <%--</div>--%>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="${pageContext.request.contextPath}/product/itemdetail?itemid=<c:out value="${item.id}"/>" class="btn btn-warning">Details</a>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-5 col-md-offset-4">
                <ul class="pagination">
                    <li><a href="itemsearch?iname=${searched}&page=0">««</a></li>
                    <c:if test="${not empty previousPage}">
                        <li><a href="itemsearch?iname=${searched}&page=${previousPage}">${previousPage}</a></li>
                    </c:if>
                    <li class="active"><a href="">${currentPage}<span class="sr-only">(current)</span></a></li>
                    <c:if test="${not empty nextPage}">
                        <li><a href="itemsearch?iname=${searched}&page=${nextPage}">${nextPage}</a></li>
                    </c:if>
                    <c:if test="${not empty lastPage}">
                        <li><a href="itemsearch?iname=${searched}&page=${lastPage}">${lastPage}</a></li>
                    </c:if>
                </ul>

            </div>
        </div>

    </div>
</c:if>

<%@ include file="../global/footer.jsp" %>

