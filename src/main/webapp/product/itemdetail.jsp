<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="../global/header.jsp" %>


<div class="container">
    <br>

    <c:if test="${not empty fail}">
        <div class="alert alert-danger" role="alert">
                ${fail}
        </div>
    </c:if>

    <c:if test="${not empty success}">
        <div class="alert alert-success" role="alert">
                ${success}
        </div>
    </c:if>

    <div class="card border-0">
        <div class="row">
            <aside class="col-lg-4">
                <article class="gallery-wrap">
                    <div class="img-big-wrap">
                        <div><img
                                src="https://via.placeholder.com/450x450?text=ProductId <c:out value="${ itemDetail.id } image " />">
                        </div>
                    </div>
                    <div class="img-small-wrap">
                        <div class="item-gallery"><img
                                src="https://via.placeholder.com/100x100?text=ProductId <c:out value="${ itemDetail.id } image " />">
                        </div>
                        <div class="item-gallery"><img
                                src="https://via.placeholder.com/100x100?text=ProductId <c:out value="${ itemDetail.id } image " />">
                        </div>
                        <div class="item-gallery"><img
                                src="https://via.placeholder.com/100x100?text=ProductId <c:out value="${ itemDetail.id } image " />">
                        </div>
                        <div class="item-gallery"><img
                                src="https://via.placeholder.com/100x100?text=ProductId <c:out value="${ itemDetail.id } image " />">
                        </div>
                    </div>
                </article>
            </aside>
            <aside class="col-sm-5">
                <article class="card-body m-0 pt-0 pl-5">
                    <h3 class="title text-uppercase">${itemDetail.name}</h3>
                    <div class="mb-3 mt-3">
                        <span class="h7 text-success">Available.</span>
                    </div>
                    <div class="mb-3 mt-3">
                        <span class="price-title">Price :</span>
                        <span class="price color-price-waanbii">${itemDetail.price}<sup>€</sup></span>
                    </div>
                    <dl class="item-property">
                        <dt>Description</dt>
                        <dd><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce congue, urna non congue
                            lacinia, dui quam hendrerit sem, vitae faucibus nulla mauris rutrum eros. Morbi elit risus,
                            efficitur sit amet orci eu, lobortis consectetur lectus. Morbi vitae dolor justo. Nullam
                            cursus tempor arcu ac suscipit. Morbi in fermentum lectus. Sed porttitor lectus cursus,
                            posuere lectus non, vestibulum diam. Etiam quis mattis leo. Vivamus laoreet libero convallis
                            justo varius mattis. Pellentesque vitae purus sit amet dui lobortis semper. Cras ac massa
                            sagittis turpis varius vulputate. Mauris viverra tellus ante, vel ultrices ex malesuada vel.
                            Morbi eu metus eu nisi bibendum efficitur at eget arcu.</p>

                            <p>Aenean laoreet convallis dui, ac interdum justo gravida eget. Aenean a faucibus nibh. Nam
                                pulvinar viverra risus, et mattis ligula viverra a. Ut rutrum erat ultricies leo rutrum
                                interdum. Fusce in mauris congue, feugiat enim facilisis, ullamcorper ipsum. Sed aliquet
                                ex vel libero mollis rutrum. Praesent at scelerisque ligula. Duis interdum vehicula
                                dolor, nec euismod sem auctor non. Class aptent taciti sociosqu ad litora torquent per
                                conubia nostra, per inceptos himenaeos. Phasellus ut arcu luctus, mollis augue non,
                                elementum lacus. Curabitur auctor lobortis leo ut congue. </p></dd>
                    </dl>
                </article>
            </aside>

            <form method="post" action="" id="form1">
                <aside class="col-sm-3">
                    <div class="row">
                        <dl class="param param-inline">
                            <dt>Quantity:</dt>
                            <dd>
                                <select name="quantity" id="quantity" form="form1" class="form-control form-control-sm"
                                        style="width:70px;">
                                    <option> 1</option>
                                    <option> 2</option>
                                    <option> 3</option>
                                    <option> 4</option>
                                    <option> 5</option>
                                    <option> 6</option>
                                    <option> 7</option>
                                    <option> 8</option>
                                    <option> 9</option>
                                    <option> 10</option>
                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="row ">
                        <button type="submit" form="form1" class="btn btn-lg color-box-waanbii" type="button"><i
                                class="fa fa-shopping-cart"></i> Add to cart
                        </button>
                    </div>
                    <div class="row mb-4 mt-4">
                        <a href="${pageContext.request.contextPath}/auth/secure/basket">
                            <button class="btn btn-lg color-box-waanbii" type="button"><i class="fa fa-credit-card"></i>
                                Go to basket
                            </button>
                        </a>

                    </div>

                </aside>
            </form>
        </div>
    </div>


</div>
<%@ include file="../global/footer.jsp" %>


