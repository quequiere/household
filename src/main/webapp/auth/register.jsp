<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="../global/header.jsp" %>


<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Auth Page</title>
</head>
<div class="container">
    <div class="row main">
        <div class="main-login main-center">
            <form class="form-horizontal" method="post" action="../auth/register" id="form1">
                <div class="panel-title text-center">
                    <h1 class="title">Register</h1>

                    <c:if test="${not empty fail}">
                        <div class="alert alert-danger" role="alert">
                                ${fail}
                        </div>
                    </c:if>

                </div>



                <div class="form-group">
                    <label for="username" class="cols-sm-2 control-label">Username</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username" required/>
                        </div>
                    </div>
                </div>



                <div class="form-group">
                    <label for="email" class="cols-sm-2 control-label">Your Email</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email" required/>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="firstname" class="cols-sm-2 control-label">Firstname</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="firstname" id="firstname"  placeholder="Enter your Firstname" required/>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="lastname" class="cols-sm-2 control-label">Lastname</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="lastname" id="lastname"  placeholder="Enter your Lastname" required/>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="postalcode" class="cols-sm-2 control-label">Postalcode</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="number" class="form-control" name="postalcode" id="postalcode"  placeholder="Enter your postalcode" required/>
                        </div>
                    </div>
                </div>




                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password" required/>
                        </div>
                    </div>
                </div>



                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password" oninput="check(this)" required/>
                        </div>
                    </div>
                </div>




                <div class="form-group ">
                    <button type="submit" form="form1" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                </div>
                <div class="login-register">
                    <a href="../auth/login">Login</a>
                </div>
            </form>
        </div>
    </div>
</div>


<script language='javascript' type='text/javascript'>
    function check(input) {
        if (input.value != document.getElementById('password').value) {
            input.setCustomValidity('Password Must be Matching.');
        } else {
            input.setCustomValidity('');
        }
    }
</script>

<%@ include file="../global/footer.jsp" %>