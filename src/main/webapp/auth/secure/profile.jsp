<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="../../global/header.jsp" %>

<body>
<div class="container">
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">

            <c:if test="${not empty fail}">
                <div class="alert alert-danger" role="alert">
                        ${fail}
                </div>
            </c:if>

            <c:if test="${not empty success}">
                <div class="alert alert-success" role="alert">
                        ${success}
                </div>
            </c:if>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">${user.username}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"><img alt="User Pic"
                                                                            src="https://via.placeholder.com/300x300?text=Image%20profile"
                                                                            class="img-circle img-responsive"></div>


                        <form class="form-horizontal" method="post" action="../secure/profile" id="form1">
                            <div class="col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>Username</td>
                                        <td>${user.username}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td class="datadisplay">${user.email}</td>
                                        <td class="datainput" style="display: none"><input type="email" class="form-control" name="email" id="email" value="${user.email}" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Firstname</td>
                                        <td class="datadisplay">${user.firstname}</td>
                                        <td class="datainput" style="display: none"><input type="text" class="form-control" name="firstname" id="firstname" value="${user.firstname}" required/></td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <td>Lastname</td>
                                        <td class="datadisplay">${user.lastname}</td>
                                        <td class="datainput" style="display: none"><input type="text" class="form-control" name="lastname" id="lastname" value="${user.lastname}" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Postalcode</td>
                                        <td class="datadisplay">${user.postalcode}</td>
                                        <td class="datainput" style="display: none"><input type="number" class="form-control" name="postalcode" id="postalcode" value="${user.postalcode}" required/></td>
                                    </tr>


                                    <tr style="display: none"  class="datainput">
                                        <td>New password (optional)</td>
                                        <td><input type="password" class="form-control" name="password" id="password"  placeholder="Type your Password"/></td>
                                    </tr>
                                    <tr style="display: none"  class="datainput">
                                        <td>Verify password</td>
                                        <td><input type="password" class="form-control" name="password1" id="password1"  placeholder="Verify your Password" oninput="check(this)"/></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </form>



                    </div>
                </div>
                <div class="panel-footer">
                    <input type="button" value="Edit profile" id="editprofile" class="btn btn-sm btn-warning datadisplay">

                    <input type="button" value="Cancel" id="cancelEdit" class="btn btn-sm btn-danger datainput" style="display: none">

                    <span class="pull-right">
                        <button type="submit" form="form1" class="btn btn-sm btn-success datainput" style="display: none">Save</button>
                    </span>

                </div>

            </div>
        </div>
    </div>
</div>
</body>

<script language='javascript' type='text/javascript'>
    $(document).ready(function () {
        $('#editprofile').click(function () {

            var data = document.getElementsByClassName("datadisplay");
            var input = document.getElementsByClassName("datainput");
            var i;
            for (i = 0; i < data.length; i++) {
                data[i].style.display = "none";

            }
            for (i = 0; i < input.length; i++) {
                input[i].style.display = "";
            }

        });
    });

    $(document).ready(function () {
        $('#cancelEdit').click(function () {

            var data = document.getElementsByClassName("datadisplay");
            var input = document.getElementsByClassName("datainput");
            var i;
            for (i = 0; i < data.length; i++) {
                data[i].style.display = "";

            }
            for (i = 0; i < input.length; i++) {
                input[i].style.display = "none";
            }

        });
    });

    function check(input) {
        if (input.value != document.getElementById('password').value) {
            input.setCustomValidity('Password Must be Matching.');
        } else {
            input.setCustomValidity('');
        }
    }
</script>

<%@ include file="../../global/footer.jsp" %>