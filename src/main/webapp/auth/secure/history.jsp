<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.js"></script>--%>
<%--<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>--%>
<%@ include file="../../global/header.jsp" %>



<div class="container">


    <c:if test="${not empty emptyH}">
        <h1 class="mainTit" style="text-align: center">You haven't bought anything ... what are you waiting for ?</h1>
    </c:if>

    <c:if test="${empty emptyH}">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h5><span class="glyphicon glyphicon-shopping-cart"></span> Last purchases </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">

                        <c:forEach items="${histories}" var="map">

                            <div class="col-xs-12 date-display"> <h4><strong>Bought the ${map.key}</strong></h4></div>


                             <c:forEach items="${map.value}" var="histo">

                            <div class="row">
                                <div class="col-xs-2"><img class="img-responsive" src="https://via.placeholder.com/100x70">
                                </div>
                                <div class="col-xs-4">
                                    <h4 class="product-name"><strong><c:out value="${histo.item.name}"/></strong></h4>
                                </div>
                                <div class="col-xs-6">
                                    <div class="col-xs-6 text-right">
                                        <h6><strong><c:out value="${histo.item.price} €"/> <span class="text-muted"> X <c:out value="${histo.quantity}"/></span></strong></h6>
                                    </div>
                                </div>
                            </div>
                            <hr>


                            </c:forEach>
                        </c:forEach>

                    </div>

                </div>
            </div>
        </div>
        </div>

    </c:if>

<%@ include file="../../global/footer.jsp" %>