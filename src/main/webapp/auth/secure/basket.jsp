<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/bootstrap.js"></script>--%>
<%--<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>--%>
<%@ include file="../../global/header.jsp" %>



<div class="container">

    <c:if test="${not empty fail}">
        <div class="alert alert-danger" role="alert">
                ${fail}
        </div>
    </c:if>

    <c:if test="${not empty success}">
        <div class="alert alert-success" role="alert">
                ${success}
        </div>
    </c:if>

    <c:if test="${not empty emptyCart}">
        <h1 class="mainTit" style="text-align: center">Your cart is empty !</h1>
    </c:if>

    <c:if test="${empty emptyCart}">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">

                        <c:forEach items="${list}" var="brow">
                            <div class="row">
                                <div class="col-xs-2"><img class="img-responsive" src="https://via.placeholder.com/100x70">
                                </div>
                                <div class="col-xs-4">
                                    <h4 class="product-name"><strong><c:out value="${brow.item.name}"/></strong></h4><h4><small>Available !</small></h4>
                                </div>
                                <div class="col-xs-6">
                                    <div class="col-xs-6 text-right">
                                        <h6><strong><c:out value="${brow.item.price} €"/> <span class="text-muted"> X <c:out value="${brow.quantity}"/></span></strong></h6>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="?delete=<c:out value="${brow.item.id}"/>">
                                            <button type="button" class="btn btn-link btn-xs">
                                                <span class="glyphicon glyphicon-trash"> </span>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </c:forEach>

                    </div


                    <div class="panel-footer">
                        <div class="row text-center">
                            <div class="col-xs-9">
                                <h4 class="text-right">Total <strong>${totalPrice} €</strong></h4>
                            </div>
                            <div class="col-xs-3">
                                <button type="button" class="btn btn-success btn-block"  data-target="#confirm-buy"  data-toggle="modal" >
                                    Pay
                                </button>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        </div>

    </c:if>





<div class="modal fade" id="confirm-buy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm buy cart</h4>
            </div>

            <div class="modal-body">
                <p>You will bough your entire cart for ${totalPrice}</p>
                <p>Are you sure ?</p>
                <p class="debug-url"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-success btn-ok" href="?buy=ok">PAY</a>
            </div>
        </div>
    </div>
</div>
<%@ include file="../../global/footer.jsp" %>