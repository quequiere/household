package com.supinfo.bruno.suphousehold.servlets.filters;

import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/auth/secure/*")
public class AuthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {


        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpSession session = req.getSession();
        final HttpServletResponse resp = (HttpServletResponse) response;

        if (session != null && session.getAttribute("username") != null)
        {
            String username = (String) session.getAttribute("username");
            User user = DaoFactory.getUserDao().findUserByName(username);

            if(user == null)
            {
               //Cause that means that the user was deleted from DB so force the logout
                resp.sendRedirect(request.getServletContext().getContextPath() + "/auth/logout");
            }
            else
            {
                request.setAttribute("user",user);
                chain.doFilter(request, response);
            }

        } else {
            resp.sendRedirect(request.getServletContext().getContextPath() + "/auth/login");
        }

    }

    @Override
    public void destroy() {

    }
}
