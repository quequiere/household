package com.supinfo.bruno.suphousehold.servlets.auth.secure;

import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.BasketRow;
import com.supinfo.bruno.suphousehold.models.Item;
import com.supinfo.bruno.suphousehold.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/auth/secure/basket")
public class BasketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public void baseDisplay(HttpServletRequest request, HttpServletResponse response)
	{
		String username = (String) request.getSession().getAttribute("username");

		if (username== null) {
			//We are not supposed to be here cause filter protected this part
			request.setAttribute("fail", "Error, you need to be logged in to do that.");
			return;
		}

		User u = DaoFactory.getUserDao().findUserByName(username);



		final String deleteItem = request.getParameter("delete");
		final String buy = request.getParameter("buy");

		if(deleteItem!=null && !deleteItem.isEmpty())
		{
			try {
				int itemId = Integer.parseInt(deleteItem);
				Item i = DaoFactory.getItemDao().getItemByID(itemId);

				if(i!=null)
				{
					DaoFactory.getBasketDao().removeItemFromBasket(u,i);
					request.setAttribute("success", "Item successfully removed from cart");
				}
				else {
					request.setAttribute("fail", "This item id doesn't exist.");
				}
			}
			catch (NumberFormatException e)
			{
				request.setAttribute("fail", "Malformed item id for deleting");
			}
		}
		else if(buy!=null && !buy.isEmpty())
		{
			List list = DaoFactory.getBasketDao().getBasketRows(u);
			DaoFactory.getHistoryDao().saveToHistory(list);
			DaoFactory.getBasketDao().clearBasket(u);
			request.setAttribute("success", "Successfully bought cart !");
		}


		List<BasketRow> list = DaoFactory.getBasketDao().getBasketRows(u);
		double price = list.stream().mapToDouble(i -> ( i.getItem().price * i.getQuantity()) ).sum();

		price = ( (int) (price * 100.0) /100.0);


		if(list == null || list.size()<=0)
		{
			request.setAttribute("emptyCart", "emptyCart");
		}
		request.setAttribute("list", list);
		request.setAttribute("totalPrice", price);
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		baseDisplay(request,response);
		request.getRequestDispatcher("/auth/secure/basket.jsp").forward(request,response);
	}


}
