package com.supinfo.bruno.suphousehold.servlets.auth;

import com.supinfo.bruno.suphousehold.component.HashComponent;
import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/auth/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		final RequestDispatcher rd = request.getRequestDispatcher("/auth/register.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String email = request.getParameter("email");
		String fname = request.getParameter("firstname");
		String lname = request.getParameter("lastname");
		String postalcode = request.getParameter("postalcode");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confirm = request.getParameter("confirm");


		if(email==null||username==null||password==null||confirm==null|| fname == null || lname == null || postalcode == null ||
				 email.isEmpty() || username.isEmpty() || password.isEmpty() || confirm.isEmpty() || fname.isEmpty() || lname.isEmpty() || postalcode.isEmpty() )
		{
			request.setAttribute("fail","Error, all value must be provided");
			request.getRequestDispatcher("/auth/register.jsp").forward(request,response);
			return;
		}

		if(!password.equals(confirm))
		{
			request.setAttribute("fail","Error, password doesn't matching");
			request.getRequestDispatcher("/auth/register.jsp").forward(request,response);
			return;
		}

		User u = DaoFactory.getUserDao().findUserByName(username);

		if(u!=null)
		{
			request.setAttribute("fail","Error, this username already exist");
			request.getRequestDispatcher("/auth/register.jsp").forward(request,response);
			return;
		}


		try
		{
			DaoFactory.getUserDao().createUser(new User(email, HashComponent.hash(password),username,fname,lname,postalcode));
			request.setAttribute("success","Successfuly created account !");
		}
		catch (Exception e)
		{
			request.setAttribute("fail",":( Error, an internal problem has occurred");
		}

		request.getSession().setAttribute("username",username);
		response.sendRedirect(request.getServletContext().getContextPath() + "/auth/secure/profile");
	}

}
