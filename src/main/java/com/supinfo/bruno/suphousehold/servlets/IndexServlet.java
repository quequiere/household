package com.supinfo.bruno.suphousehold.servlets;

import com.supinfo.bruno.suphousehold.dao.DaoFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/index")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setAttribute("users", DaoFactory.getUserDao().getUserCount());
		request.setAttribute("items", DaoFactory.getItemDao().getItemCount());
		request.setAttribute("histories", DaoFactory.getHistoryDao().getTotalRow());
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}


}
