package com.supinfo.bruno.suphousehold.servlets.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/auth/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(request.getSession().getAttribute("username")!=null)
		{
			request.setAttribute("success","Successfully logged out !");
			request.getSession().removeAttribute("username");
		}
		else
			request.setAttribute("fail","You was not logged");

		request.getRequestDispatcher("/auth/login.jsp").forward(request,response);
	}
}
