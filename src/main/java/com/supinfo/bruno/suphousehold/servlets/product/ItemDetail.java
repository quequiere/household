package com.supinfo.bruno.suphousehold.servlets.product;

import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.Item;
import com.supinfo.bruno.suphousehold.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/product/itemdetail")
public class ItemDetail extends HttpServlet {
    private static final long serialVersionUID = 1L;


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        displayBasicPage(request,response);
    }

    public void displayBasicPage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        final String itemIdParam = request.getParameter("itemid");
        if (itemIdParam == null || itemIdParam.isEmpty()) {
            response.sendRedirect(request.getServletContext().getContextPath() + "/");
            return;
        }

        int itemId = 0;

        try {
            itemId = Integer.parseInt(itemIdParam);

        } catch (NumberFormatException e) {
            response.sendRedirect(request.getServletContext().getContextPath() + "/");
            return;
        }

        Item i = DaoFactory.getItemDao().getItemByID(itemId);

        if (i == null) {
            response.sendRedirect(request.getServletContext().getContextPath() + "/");
            return;
        }

        request.setAttribute("itemDetail", i);

        request.getRequestDispatcher("/product/itemdetail.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = (String) request.getSession().getAttribute("username");

        if (username== null) {
            request.setAttribute("fail", "Error, you need to be logged in to do that.");
            displayBasicPage(request,response);
            return;
        }

        User u = DaoFactory.getUserDao().findUserByName(username);

        if(u == null)
        {
            request.setAttribute("fail", "Error, we can't find your account. Try to logout.");
            displayBasicPage(request,response);
            return;
        }


        final String itemIdString = request.getParameter("itemid");
        final String quantityString = request.getParameter("quantity");

        if (itemIdString == null || itemIdString.isEmpty() || quantityString == null || quantityString.isEmpty()) {
            request.setAttribute("fail", "Error, some parametters are missing");
            displayBasicPage(request,response);
            return;
        }


        int quantity;
        int itemId;
        try {
            quantity = Integer.parseInt(quantityString);
            itemId = Integer.parseInt(itemIdString);

        } catch (NumberFormatException e) {
            request.setAttribute("fail", "Error, malformed request");
            displayBasicPage(request,response);
            return;
        }

        Item i = DaoFactory.getItemDao().getItemByID(itemId);

        if(i == null)
        {
            request.setAttribute("fail", "Error, item doesn't exist in database");
            displayBasicPage(request,response);
            return;
        }


        if( DaoFactory.getBasketDao().insertNewItemInBasket(u,i,quantity))
        {
            request.setAttribute("success", "Item added to basket");
            displayBasicPage(request,response);
        }
        else {
            request.setAttribute("fail", "Error, while adding new item into basket");
            displayBasicPage(request,response);  displayBasicPage(request,response);
        }
    }
}
