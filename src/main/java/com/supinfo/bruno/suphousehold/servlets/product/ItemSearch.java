package com.supinfo.bruno.suphousehold.servlets.product;

import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.Item;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/product/itemsearch")
public class ItemSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		final String searchedName = request.getParameter("iname");
		if(searchedName == null ||searchedName.isEmpty())
		{
			response.sendRedirect(request.getServletContext().getContextPath() + "/");
			return;
		}

		List<Item> result = DaoFactory.getItemDao().findAllItemByName(searchedName);
		int totalResult = result.size();



		if(totalResult<=0)
		{
			request.setAttribute("fail","Sorry, no result found :(");
		}
		else {

			request.setAttribute("searched",searchedName);

			final String pageId = request.getParameter("page");
			int currentPage = 0;
			try
			{
				currentPage = Integer.parseInt(pageId);
			}
			catch (NumberFormatException e)
			{

			}


			int pageNumber = getTotalPage(totalResult) -1;


			if(currentPage!=0)
			{
				request.setAttribute("previousPage",currentPage-1);
			}


			if(currentPage<(pageNumber))
			{
				request.setAttribute("nextPage",currentPage+1);
			}

			request.setAttribute("currentPage",currentPage);

			if(currentPage!=pageNumber && pageNumber>1)
			{
				request.setAttribute("lastPage",pageNumber);
			}



			try
			{
				if(currentPage != pageNumber)
				{
					result = result.subList(10*currentPage,10 + (10*currentPage));
				}
				else {
					result = result.subList(10*currentPage,result.size());
				}

				request.setAttribute("iresList",result);
			}
			catch (IndexOutOfBoundsException e)
			{
				e.printStackTrace();
				request.setAttribute("fail","Can't find this result page");
			}




		}



		request.getRequestDispatcher("/product/itemsearch.jsp").forward(request,response);
	}

	private static int getTotalPage(int totalResult)
	{
		int pageNumber = (int) Math.floor(totalResult/10.0);
		int rest = totalResult % 10;

		if(rest>0)
		{
			pageNumber++;
		}
		return pageNumber;
	}
}
