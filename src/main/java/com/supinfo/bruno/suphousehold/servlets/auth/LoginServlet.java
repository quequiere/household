package com.supinfo.bruno.suphousehold.servlets.auth;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.supinfo.bruno.suphousehold.component.HashComponent;
import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.User;


@WebServlet("/auth/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final RequestDispatcher rd = request.getRequestDispatcher("/auth/login.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {
		final String username = request.getParameter("username");
		final String password = request.getParameter("password");
		
    	if(username == null && username.isEmpty() && password == null && password.isEmpty()) {
			request.setAttribute("fail","Error, all value must be completed");
			request.getRequestDispatcher("/auth/login.jsp").forward(request,response);
			return;
    	}

    	User u = DaoFactory.getUserDao().findUserByName(username);

    	if(u == null)
		{
			request.setAttribute("fail","Error, invalid username");
			request.getRequestDispatcher("/auth/login.jsp").forward(request,response);
			return;
		}

    	String givedPassword = HashComponent.hash(password);
    	if(!u.getPassword().equals(givedPassword))
		{
			request.setAttribute("fail","Error, invalid password");
			request.getRequestDispatcher("/auth/login.jsp").forward(request,response);
			return;
		}


		request.getSession().setAttribute("username",username);
		response.sendRedirect(request.getServletContext().getContextPath() + "/auth/secure/profile");
	}

}
