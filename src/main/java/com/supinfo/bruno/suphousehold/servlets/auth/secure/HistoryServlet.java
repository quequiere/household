package com.supinfo.bruno.suphousehold.servlets.auth.secure;

import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.BasketRow;
import com.supinfo.bruno.suphousehold.models.History;
import com.supinfo.bruno.suphousehold.models.Item;
import com.supinfo.bruno.suphousehold.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@WebServlet("/auth/secure/history")
public class HistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String username = (String) request.getSession().getAttribute("username");

		if (username== null) {
			//We are not supposed to be here cause filter protected this part
			response.sendRedirect(request.getServletContext().getContextPath() + "/auth/login");
			return;
		}

		User u = DaoFactory.getUserDao().findUserByName(username);

		List<History> histories = DaoFactory.getHistoryDao().getHistory(u);


		Map<String, List<History>> groupedByDate = histories.stream().collect(Collectors.groupingBy(History::formatedDate));

		if(histories==null || histories.size() <= 0)
		{
			request.setAttribute("emptyH", "empty");
		}
		else {
			request.setAttribute("histories", groupedByDate);
		}


		request.getRequestDispatcher("/auth/secure/history.jsp").forward(request,response);
	}


}
