package com.supinfo.bruno.suphousehold.servlets.auth.secure;

import com.supinfo.bruno.suphousehold.component.HashComponent;
import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/auth/secure/profile")
public class ProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;




	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/auth/secure/profile.jsp").forward(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if(request.getAttribute("user")==null) return;

		User u = (User) request.getAttribute("user");

		String email = request.getParameter("email");
		String fname = request.getParameter("firstname");
		String lname = request.getParameter("lastname");
		String postalcode = request.getParameter("postalcode");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");



		if( fname == null || lname == null || postalcode == null ||
				email.isEmpty() || fname.isEmpty() || lname.isEmpty() || postalcode.isEmpty() )
		{
			request.setAttribute("fail","Error, all value must be provided");
			request.getRequestDispatcher("/auth/secure/profile.jsp").forward(request,response);
			return;
		}

		if(password!=null && password1 !=null)
		{
				if(  !password.isEmpty() && !password1.isEmpty() && password.equals(password1))
				{
					u.setPassword(HashComponent.hash(password));
				}
				else if( !(password.isEmpty() && password1.isEmpty()) && !password.equals(password1) )
				{
					request.setAttribute("fail","Error, password doesn't matching");
					request.getRequestDispatcher("/auth/secure/profile.jsp").forward(request,response);
					return;
				}
		}
		else if(password!=null || password1 !=null) {
			request.setAttribute("fail","You must provide all password fields");
			request.getRequestDispatcher("/auth/secure/profile.jsp").forward(request,response);
			return;
		}


		u.setEmail(email);
		u.setFirstname(fname);
		u.setLastname(lname);
		u.setPostalcode(postalcode);

		try
		{
			DaoFactory.getUserDao().updateUser(u);
			request.setAttribute("success","Successfully updated profile.");

		}catch (Exception e)
		{
			request.setAttribute("fail","Failed to update user data :(");
			e.printStackTrace();
		}

		request.getRequestDispatcher("/auth/secure/profile.jsp").forward(request,response);


	}
}
