package com.supinfo.bruno.suphousehold.component;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class NameGeneratorComponent {

    public static String[] getItemsName()   {

        InputStream stream = NameGeneratorComponent.class.getResourceAsStream("/extract.txt");
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8.name()))) {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }

        return stringBuilder.toString().split("#");
    }
}
