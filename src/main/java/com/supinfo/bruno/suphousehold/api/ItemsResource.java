package com.supinfo.bruno.suphousehold.api;

import com.google.gson.Gson;
import com.supinfo.bruno.suphousehold.dao.DaoFactory;
import com.supinfo.bruno.suphousehold.models.Item;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/items")
public class ItemsResource {

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllItems(){

        return Response.ok(new Gson().toJson(DaoFactory.getItemDao().getAllItem()), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/{itemId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response search(@PathParam("itemId") int id){

       Item result =  DaoFactory.getItemDao().getItemByID(id);
       if(result !=null)
       {
           return Response.ok(new Gson().toJson(result), MediaType.APPLICATION_JSON).build();
       }
       else {
           return Response.status(Response.Status.NOT_FOUND).entity("Item not found for id: " + id).build();
       }

    }

    @GET
    @Path("/search/{itemName}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response search(@PathParam("itemName") String name){

        List<Item> result =  DaoFactory.getItemDao().findItemByName(name);
        return Response.ok(new Gson().toJson(result), MediaType.APPLICATION_JSON).build();

    }

    @GET
    @Path("/searchName/{itemName}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response searchName(@PathParam("itemName") String name){

        List<String> result =  DaoFactory.getItemDao().findItemName(name);
        return Response.ok(new Gson().toJson(result), MediaType.APPLICATION_JSON).build();

    }

}