package com.supinfo.bruno.suphousehold.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="SupHouseBasket")
public class BasketRow implements Serializable {

    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
            // This seems to doesn't word for mysqlite. The table can't be create itself from jpa
    long idRow;

    @OneToOne
    @JoinColumn(name = "username")
    private User owner;

    @OneToOne
    @JoinColumn(name = "itemId")
    private Item item;

    private int quantity;


    public BasketRow(long rowId, User owner, Item item, int quantity) {
        this.idRow=rowId;
        this.owner = owner;
        this.item = item;
        this.quantity = quantity;
    }

    public BasketRow() {
    }


    public long getId() {
        return idRow;
    }

    public User getOwner() {
        return owner;
    }

    public Item getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
