package com.supinfo.bruno.suphousehold.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="SuphouseItem")
public class Item implements Serializable {

    public String name;
    @Id
    public int itemId;
    public double price;

    public Item(String name, int id, double price) {
        this.name = name;
        this.itemId = id;
        this.price = price;
    }

    public Item() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return itemId;
    }

    public void setId(int id) {
        this.itemId = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
