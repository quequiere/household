package com.supinfo.bruno.suphousehold.models;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name="SupHouseHistory")
public class History implements Serializable {

    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    // This seems to doesn't word for mysqlite. The table can't be create itself from jpa
    long idRow;

    @OneToOne
    @JoinColumn(name = "username")
    private User owner;

    @OneToOne
    @JoinColumn(name = "itemId")
    private Item item;

    private int quantity;

    @Temporal(TemporalType.TIMESTAMP)
    private Date boughtTime;


    public History(long idRow, User owner, Item item, int quantity, Date boughtTime) {
        this.idRow = idRow;
        this.owner = owner;
        this.item = item;
        this.quantity = quantity;
        this.boughtTime = boughtTime;
    }

    public History() {
    }

    public long getIdRow() {
        return idRow;
    }

    public User getOwner() {
        return owner;
    }

    public Item getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public Date getBoughtTime() {
        return boughtTime;
    }
    public String formatedDate() {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy 'at' HH:mm:ss");
        return formater.format(boughtTime);
    }
}
