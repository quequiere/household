package com.supinfo.bruno.suphousehold.dao.objects;

import com.supinfo.bruno.suphousehold.models.BasketRow;
import com.supinfo.bruno.suphousehold.models.Item;
import com.supinfo.bruno.suphousehold.models.User;

import java.util.List;

public interface IBasketDao
{
	 boolean insertNewItemInBasket(User u, Item i, int quantity);
	 BasketRow getBasketRow(User u, Item i);
	 void removeItemFromBasket(User u, Item i);
	 void clearBasket(User u);
	 long getTotalRow();
	 List<BasketRow> getBasketRows(User u);

}
