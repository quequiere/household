package com.supinfo.bruno.suphousehold.dao.objects;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import com.supinfo.bruno.suphousehold.models.User;


public class UserDao implements IUserDao {

	private EntityManagerFactory emf;
	
	public UserDao(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	@Override
	public void createUser(User user) {

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(user);
		em.getTransaction().commit();
	}

	@Override
	public User findUserByName(String userName) {
		try
		{
			EntityManager em = emf.createEntityManager();
			Query query = em.createQuery("SELECT p FROM User AS p WHERE p.username = :uname");
			query.setParameter("uname", userName);
			final User user = (User) query.getSingleResult();
			em.close();
			return user;
		}
		catch (NoResultException e)
		{
			return null;
		}

	}

	@Override
	public void updateUser(User user) {

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(user);
		em.getTransaction().commit();
	}

	@Override
	public long getUserCount() {
		try
		{
			EntityManager em = emf.createEntityManager();
			Query query = em.createQuery("SELECT COUNT(*) FROM User ");
			long result = (Long) query.getSingleResult();
			return result;
		}catch (Exception e)
		{
			return 0;
		}

	}

}


