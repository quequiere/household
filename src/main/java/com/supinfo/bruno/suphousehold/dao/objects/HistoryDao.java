package com.supinfo.bruno.suphousehold.dao.objects;

import com.supinfo.bruno.suphousehold.models.BasketRow;
import com.supinfo.bruno.suphousehold.models.History;
import com.supinfo.bruno.suphousehold.models.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class HistoryDao implements IHistoryDao {

    private EntityManagerFactory emf;

    public HistoryDao(EntityManagerFactory emf) {
        super();
        this.emf = emf;
    }

    @Override
    public long getTotalRow() {
        try {
            EntityManager em = emf.createEntityManager();
            Query query = em.createQuery("SELECT COUNT(*) FROM History ");
            long result = (Long) query.getSingleResult();
            return result;
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public void saveToHistory(List<BasketRow> rows) {
        long lastCount = this.getTotalRow();

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Date time = Calendar.getInstance().getTime();

        for (BasketRow r : rows) {
            History h = new History(lastCount, r.getOwner(), r.getItem(), r.getQuantity(), time);
            em.persist(h);
            lastCount++;
        }

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<History> getHistory(User u) {
        try {
            EntityManager em = emf.createEntityManager();
            Query query = em.createQuery("SELECT h FROM History AS h WHERE h.owner = :u");
            query.setParameter("u", u);
            final List<History> basketRows = (List<History>) query.getResultList();
            em.close();
            return basketRows;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}


