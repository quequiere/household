package com.supinfo.bruno.suphousehold.dao.objects;

import com.supinfo.bruno.suphousehold.models.BasketRow;
import com.supinfo.bruno.suphousehold.models.History;
import com.supinfo.bruno.suphousehold.models.User;

import java.util.List;

public interface IHistoryDao
{
	 long getTotalRow();
	 void saveToHistory(List<BasketRow> rows);
	 List<History> getHistory(User u);
}
