package com.supinfo.bruno.suphousehold.dao.objects;

import com.supinfo.bruno.suphousehold.component.NameGeneratorComponent;
import com.supinfo.bruno.suphousehold.models.Item;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;
import java.util.Random;


public class ItemDao implements IItemDao {

	private EntityManagerFactory emf;

	public ItemDao(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	@Override
	public void initializeData() {

		System.out.println("Creating fake data in DB ....");
		Random r = new Random();
		double min = 0.01;
		double max = 999;

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();

		String[] itemsName = NameGeneratorComponent.getItemsName();

		if(getItemCount()<=0)
		{
			for(int x=0; x<itemsName.length;x++)
			{
				double randomPrice = min + (max - min) * r.nextDouble();
				randomPrice = ((int) (randomPrice * 100)/100.0);
				em.persist(new Item(itemsName[x],x,randomPrice));
			}
		}
		em.getTransaction().commit();

		System.out.println(getItemCount()+" random data writed to database");
	}

	@Override
	public Item getItemByID(int id) {
		try
		{
			EntityManager em = emf.createEntityManager();
			Query query = em.createQuery("SELECT i FROM Item AS i WHERE i.id = :iId");
			query.setParameter("iId", id);
			final Item item = (Item) query.getSingleResult();
			em.close();
			return item;
		}
		catch (Exception e)
		{
			return null;
		}

	}

	@Override
	public List<Item> findItemByName(String name) {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT i FROM Item AS i WHERE i.name LIKE :iname ORDER BY i.name ");
		query.setParameter("iname", "%"+name+"%");
		query.setMaxResults(10);
		return query.getResultList();
	}

	@Override
	public List<Item> findAllItemByName(String name) {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT i FROM Item AS i WHERE i.name LIKE :iname ORDER BY i.name");
		query.setParameter("iname", "%"+name+"%");
		return query.getResultList();
	}

	@Override
	public List<String> findItemName(String name) {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT i.name FROM Item AS i WHERE i.name LIKE :iname ORDER BY i.name");
		query.setParameter("iname", "%"+name+"%");
		query.setMaxResults(10);
		return query.getResultList();
	}

	@Override
	public List<Item> getAllItem() {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT i FROM Item as i");
		return query.getResultList();
	}

	@Override
	public long getItemCount() {

		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT COUNT(*) FROM Item");
		long result = (Long) query.getSingleResult();
		return result;
	}
}


