package com.supinfo.bruno.suphousehold.dao.objects;

import com.supinfo.bruno.suphousehold.models.User;

public interface IUserDao {
	void createUser(final User user);
	User findUserByName(final String name);
	void updateUser(final User user);
	long getUserCount();
}
