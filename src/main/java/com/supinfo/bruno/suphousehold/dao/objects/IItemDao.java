package com.supinfo.bruno.suphousehold.dao.objects;

import com.supinfo.bruno.suphousehold.models.Item;

import java.util.List;

public interface IItemDao
{
	void initializeData();
	Item getItemByID(int id);
	List<Item> findItemByName(String name);
	List<Item> findAllItemByName(String name);
	List<Item> getAllItem();
	long getItemCount();
	List<String> findItemName(String name);
}
