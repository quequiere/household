package com.supinfo.bruno.suphousehold.dao;

import com.supinfo.bruno.suphousehold.dao.objects.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoFactory {

    private static EntityManagerFactory emf;
    private DaoFactory() {
    }

    public static IBasketDao getBasketDao() {
        return new BasketDao(getConnection());
    }

    public static IHistoryDao getHistoryDao() {
        return new HistoryDao(getConnection());
    }

    public static IUserDao getUserDao() {

        return new UserDao(getConnection());
    }

    public static IItemDao getItemDao() {

        ItemDao dao = new ItemDao(getConnection());
        if (dao.getItemCount()<=0) {
            dao.initializeData();
        }

        return dao;
    }

    public static EntityManagerFactory getConnection() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("SupHouseHold");
        }
        return emf;
    }

    public static void close() {
        if (emf != null && emf.isOpen()) {
            emf.close();
        }
    }
}
