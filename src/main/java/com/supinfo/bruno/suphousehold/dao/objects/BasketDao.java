package com.supinfo.bruno.suphousehold.dao.objects;

import com.supinfo.bruno.suphousehold.models.BasketRow;
import com.supinfo.bruno.suphousehold.models.Item;
import com.supinfo.bruno.suphousehold.models.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


public class BasketDao implements IBasketDao {

	private EntityManagerFactory emf;

	public BasketDao(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	@Override
	public boolean insertNewItemInBasket(User u, Item i, int quantity) {

		if(quantity<=0)
		{
			System.out.println("Error, quantity can't be inferio or equal to 0");
			return false;
		}

		BasketRow oldRow = getBasketRow(u,i);

		if(oldRow!=null)
		{
			int newQuantity = oldRow.getQuantity() + quantity;

			EntityManager em = emf.createEntityManager();

			BasketRow iEntity = em.find(oldRow.getClass(),oldRow.getId());

			em.getTransaction().begin();
			iEntity.setQuantity(newQuantity);
			em.getTransaction().commit();
		}
		else {
			long newId = this.getTotalRow() + 1;
			BasketRow newRow = new BasketRow(newId,u,i,quantity);

			EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();
			em.persist(newRow);
			em.getTransaction().commit();

		}

		return true;
	}

	@Override
	public BasketRow getBasketRow(User u, Item i) {
		try
		{
			EntityManager em = emf.createEntityManager();
			Query query = em.createQuery("SELECT br FROM BasketRow AS br WHERE br.owner = :u AND br.item = :i");
			query.setParameter("u", u);
			query.setParameter("i", i);
			final BasketRow basketRow = (BasketRow) query.getSingleResult();
			em.close();
			return basketRow;
		}
		catch (Exception e)
		{
			return null;
		}
	}

	@Override
	public void removeItemFromBasket(User u, Item i) {
		EntityManager em = emf.createEntityManager();
		Query querry = em.createQuery("DELETE FROM BasketRow r WHERE r.item = :item AND r.owner = :owner");
		querry.setParameter("item", i);
		querry.setParameter("owner", u);
		em.getTransaction().begin();
		querry.executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void clearBasket(User u) {
		EntityManager em = emf.createEntityManager();
		Query querry = em.createQuery("DELETE FROM BasketRow r WHERE r.owner = :owner");
		querry.setParameter("owner", u);
		em.getTransaction().begin();
		querry.executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public long getTotalRow() {
		try
		{
			EntityManager em = emf.createEntityManager();
			Query query = em.createQuery("SELECT COUNT(*) FROM BasketRow");
			long result = (Long) query.getSingleResult();
			return result;
		}catch (Exception e)
		{
			return 0;
		}

	}

	@Override
	public List<BasketRow> getBasketRows(User u) {
		try
		{
			EntityManager em = emf.createEntityManager();
			Query query = em.createQuery("SELECT br FROM BasketRow AS br WHERE br.owner = :u");
			query.setParameter("u", u);
			final List<BasketRow> basketRows = (List<BasketRow>) query.getResultList();
			em.close();
			return basketRows;
		}
		catch (Exception e)
		{
			return new ArrayList<>();
		}
	}
}


