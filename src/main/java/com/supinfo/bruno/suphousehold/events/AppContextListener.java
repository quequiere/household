package com.supinfo.bruno.suphousehold.events;

import com.supinfo.bruno.suphousehold.dao.DaoFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {


    public AppContextListener() {
    }

    public void contextDestroyed(ServletContextEvent arg0)  {
        DaoFactory.close();
    }

    public void contextInitialized(ServletContextEvent arg0)  {
        System.out.println("SupHouseHold is now started !");
    }

}